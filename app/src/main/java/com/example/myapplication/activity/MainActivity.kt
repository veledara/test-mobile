package com.example.myapplication.activity

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.myapplication.R
import com.example.myapplication.util.AnimView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.net.HttpURLConnection
import java.net.URL
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class MainActivity : AppCompatActivity() {

    private lateinit var imageView: ImageView
    private lateinit var buttonUpdate: Button
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initComponents()
        applyComponents()
    }

    private fun initComponents() {
        buttonUpdate = findViewById(R.id.button_check_currency)

        imageView = findViewById(R.id.field_of_gif_image)
        progressBar = findViewById(R.id.picture_progressbar)

        showGif(if(System.currentTimeMillis() % 2L == 0L) R.drawable.zero
                else R.drawable.one, imageView, progressBar)
    }


    private fun applyComponents() {
        buttonUpdate.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                val idGif = awaitDownloadUrl().split("-").last()
                Handler(Looper.getMainLooper()).post {
                    showGif("https://media.giphy.com/media/$idGif/giphy.gif",
                    imageView, progressBar) }
            }
        }
    }

    private suspend fun awaitDownloadUrl() : String {
        return suspendCoroutine { iter ->
            val url = URL("https://apelsin-service-production.up.railway.app/currency")
            with(url.openConnection() as HttpURLConnection) {
                requestMethod = "GET"  // optional default is GET

                println("\nSent 'GET' request to URL : $url; Response Code : $responseCode")

                var resUrl = ""
                inputStream.bufferedReader().use {
                    it.lines().forEach { line ->
                        resUrl += line
                        println(line)
                    }
                    iter.resume(resUrl)
                }
            }
        }
    }

    fun showGif(res: Any, imageView : ImageView, progressBar: ProgressBar) {
        val res1 = if(res is String)  res  else res as Int

        Glide.with(this)
            .asGif()
            .load(res1)
            .error(R.drawable.zero)
            .listener(object : RequestListener<GifDrawable?> {

                override fun onResourceReady(
                    resource: GifDrawable?, model: Any?, target: Target<GifDrawable?>?,
                    dataSource: DataSource?, isFirstResource: Boolean
                ): Boolean {
                    AnimView.animVisible(imageView, 100)
                    return false
                }

                override fun onLoadFailed(e: GlideException?, model: Any?,
                    target: Target<GifDrawable?>?, isFirstResource: Boolean): Boolean {
                    AnimView.animGone(progressBar, 100)
                    AnimView.animVisible(imageView, 100)
                    return false
                }
            })
            .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
            .apply(RequestOptions.bitmapTransform(RoundedCorners(16)))
            .into(imageView)
    }

}
